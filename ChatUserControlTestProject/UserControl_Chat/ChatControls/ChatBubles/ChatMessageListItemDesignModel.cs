﻿
namespace UserControl_Chat
{
    /// <summary>
    /// The design-time data for a <see cref="ChatMessageListItemViewModel"/>
    /// </summary>
    public class ChatMessageListItemDesignModel : ChatMessageListItemViewModel
    {
        #region Singleton
        /// <summary>
        /// A single instance of the design model
        /// </summary>
        public static ChatMessageListItemDesignModel Instance => new ChatMessageListItemDesignModel();

        #endregion

        #region Constructor

        public ChatMessageListItemDesignModel()
        {
            Initials = "ST 1";
            SentByMe = Roles.SentByMe;
            Message = "Last Message from LM bla-bla-bla-lba-bla-bla-lba";
            ProfilePictureRGB = "3099c5";

        }

        #endregion
    }
}
