﻿
namespace UserControl_Chat
{
    /// <summary>
    /// The design-time data for a <see cref="ChatListItemViewModel"/>
    /// </summary>
    public class ChatListItemDesignModel : ChatListItemViewModel
    {
        #region Singleton
        /// <summary>
        /// A single instance of the design model
        /// </summary>
        public static ChatListItemDesignModel Instance => new ChatListItemDesignModel();

        #endregion

        #region Constructor

        /// <summary>
        /// example just for design
        /// </summary>
        public ChatListItemDesignModel()
        {
            Id = 9;
            //Name = "ss";
            Message = "1";
            ProfilePictureRGB = "3099c5";
        }

        #endregion
    }
}
