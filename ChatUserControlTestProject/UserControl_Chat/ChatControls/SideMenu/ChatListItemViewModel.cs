﻿
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace UserControl_Chat
{
    /// <summary>
    /// A view model for each chat list item in the overview chat list
    /// </summary>
    public class ChatListItemViewModel : BaseViewModel
    {

        /// <summary>
        /// Caption
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The latest message from this chat
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// The initials to show for the profile picture background
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The RGB values (in hex) for the background color of the profile picture
        /// For example FF00FF for Red and Blue mixed
        /// </summary>
        public string ProfilePictureRGB { get; set; }

        /// <summary>
        /// True if there are unread messages in this chat 
        /// </summary>
        //public bool NewContentAvailable { get; set; }

        /// <summary>
        /// True if this item is currently selected
        /// </summary>
        //public bool IsSelected { get; set; }

        /// <summary>
        /// The RGB values (in hex) for the background color of the strip( show that its new message)
        /// </summary>
        public string NewContentAvailableField { get; set; }

        /// <summary>
        /// The RGB values (in hex) for the background color of the chat member cell 
        /// For example FF00FF for Red and Blue mixed
        /// </summary>
        public string BackgrounField { get; set; }


        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return base.ToString();
        }
    }
}
