﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace UserControl_Chat
{
    /// <summary>
    /// Interaction logic for ChatItem.xaml
    /// </summary>
    public partial class ChatItem : UserControl
    {
        private int _selectedItemId = -1;

        public ChatItem()
        {
            //DataContext = this;
            InitializeComponent();
            Events.OnClick += ChoosenChatMemberIsActive;
            Events.OnSelectedItem += LoadSelectedItem;

        }

        private void LoadSelectedItem(int selectedItemId)
        {
            _selectedItemId = selectedItemId;
        }

        private void ChoosenChatMemberIsActive()
        {
            if (_selectedItemId == -1)
            {
                _selectedItemId = (int)NameToGetTag.Tag;
            }

            if (_selectedItemId != -1 && _selectedItemId.Equals((int)NameToGetTag.Tag))
            {
                Repainter();
            }
        }

        private void ListItemControl_MouseClick(object sender, MouseButtonEventArgs e)
        {
            Repainter();
        }

        //change color of active message to nonactive
        private void Repainter()
        {
            Events.SelectedItem((int)NameToGetTag.Tag);
        }
    }
}
